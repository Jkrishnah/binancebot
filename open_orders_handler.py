import os
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'binancebot.settings'
django.setup()
from bbot.models import *
import time
from datetime import datetime,timedelta
from Symbol import Pair
from binance.client import Client

apiKey = Key.objects.first()
client = Client(apiKey.apiKey,apiKey.apiSecret)

open_orders = client.get_open_orders()

now = time.time()
pairs = Config.objects.all()

for order in open_orders:
    orderId = order['orderId']
    orderSymbol = order['symbol']
    time = (now - int(order['time']/1000))/3600
    if pairs.get(symbol=orderSymbol).order_cancel_time <= time:
        try:
            order_cancellation = client.cancel_order(symbol=orderSymbol,orderId=orderId)
            orderObj = Spot_Order.objects.get(orderId = orderId)
            orderObj.status = 'BOT CANCELED'
            orderObj.cancel_time = datetime.utcnow()+timedelta(hours=1)
            orderObj.save()
        except Exception as e:
            Debug.objects.create(time =datetime.utcnow()+timedelta(hours=1),error = f'Open order cancellation Script - unable to cancel order {orderId} of pair {orderSymbol}')
            continue