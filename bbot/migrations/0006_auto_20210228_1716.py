# Generated by Django 3.0.7 on 2021-02-28 17:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bbot', '0005_auto_20210228_1550'),
    ]

    operations = [
        migrations.AlterField(
            model_name='debug',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2021, 2, 28, 18, 16, 26, 658356), editable=False),
        ),
        migrations.AlterField(
            model_name='order',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2021, 2, 28, 18, 16, 26, 657882), editable=False),
        ),
    ]
