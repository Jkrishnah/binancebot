from crontab import CronTab
import datetime
import croniter
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime,timedelta
from django.apps import apps
from bbot.models import APIlimit,Spot_Order,Market_Order,Config

def maximum_storage_handler(sender, instance, *args, **kwargs):
    last24hours  = ((datetime.utcnow()+timedelta(hours=1)) - timedelta(hours=24))
    APIlimitObj =  APIlimit.objects.first()
    APIlimitObj.Order_Count = len(Spot_Order.objects.filter(order_time__gte = last24hours))+len(Market_Order.objects.filter(order_time__gte = last24hours))
    APIlimitObj.save()
    if len(sender.objects.all())>10000:
        sender.objects.all().order_by('-time').last().delete()

def maximum_log_storage_handler(sender, instance, *args, **kwargs):
    if len(sender.objects.all())>10000:
        sender.objects.all().order_by('-time').last().delete()

def global_config_updater(sender, instance, *args, **kwargs):
    
    for lconfig in Config.objects.all():
        lconfig.active,lconfig.volume,lconfig.depth,lconfig.wait = instance.active , instance.volume , instance.depth , instance.wait
        lconfig.wallet_balance  = instance.wallet_balance
        lconfig.order_cancel_time = instance.order_cancel_time
        lconfig.save()


def cron_handler(sender, instance, *args, **kwargs):
    cron = CronTab(user='root')
    found=0
    for job in cron:
        if job.comment == 'binance_cron':
            if instance.run:
                job.minute.every(instance.interval/60)
                cron.write()
                found = 1
            else:
                found = 1
                cron.remove(job)
                cron.write()
    if found:
        pass
    else:
        if instance.run:
            job = cron.new(command='/usr/bin/flock -w 0 binancebot/binancebot/binance_process.lock /env/bin/python binancebot/binancebot/script.py', comment='binance_cron')
            job.minute.every(instance.interval/60)
            cron.write()
        else:
            pass

    
def order_cancel_handler(sender, instance, *args, **kwargs):
    cron = CronTab(user='root')
    found=0
    for job in cron:
        if job.comment == 'binance_cancel_cron':
            if instance.active:
                job.minute.every(instance.order_cancel_run_time)
                cron.write()
                found = 1
            else:
                found = 1
                cron.remove(job)
                cron.write()
    if found:
        pass
    else:
        if instance.run:
            job = cron.new(command='/env/bin/python binancebot/binancebot/', comment='binance_cancel_cron')
            job.every(instance.order_cancel_run_time)
            cron.write()
        else:
            pass