from django.db import models
from datetime import datetime,timedelta
from django.db.models.signals import post_save
from django.db.models.signals import pre_save

SIDE_CHOICES = [('SELL','SELL'),('BUY','BUY')]

# Create your models here.
class LastRunTime(models.Model):
    time = models.DateTimeField()
    def __str__(self):
        return f'{self.time}'
    class Meta:
        verbose_name = 'Last Run Time'
        verbose_name_plural = 'Last Run Time'
    

class Config(models.Model):
    symbol = models.CharField(max_length=30,primary_key=True)
    dominant_Pair = models.CharField(max_length=10)
    recessive_Pair =  models.CharField(max_length=10)
    volume = models.FloatField(default=None)
    depth = models.FloatField(default=None)
    wait = models.FloatField()
    active = models.BooleanField(default=False)
    wallet_balance = models.BooleanField(default=False)
    order_cancel_time = models.IntegerField(default=24)
    minimum = models.FloatField()

    def __str__(self):
        return f'{self.symbol}'
    class Meta:
        verbose_name = 'Local Pair Configuration'
        verbose_name_plural = 'Pair Configurations'


class GlobalConfig(models.Model):
    # symbol = models.CharField(max_length=10,primary_key=True)
    # dominant_Pair = models.CharField(max_length=10,default=None)
    # recessive_Pair =  models.CharField(max_length=10,default=None)
    volume = models.FloatField(default=None)
    depth = models.FloatField(default=None)
    wait = models.FloatField()
    minimum = models.FloatField()
    active = models.BooleanField(default=False)
    wallet_balance = models.BooleanField(default=False)
    order_cancel_time = models.IntegerField(null=True)
    # order_cancel_run_time = models.IntegerField()
    def __str__(self):
        return f'Global config'
    class Meta:
        verbose_name = 'Global Pair Configuration'
        verbose_name_plural = 'Global Pair Configurations'

class Key(models.Model):
    apiKey = models.CharField(max_length=600)
    apiSecret = models.CharField(max_length=600)
    def __str__(self):
        return 'API Key'
    class Meta:
        verbose_name = 'API Keys'
        verbose_name_plural = 'API Keys'

class Spot_Order(models.Model):
    symbol = models.ForeignKey(Config,on_delete=models.CASCADE)
    orderId = models.CharField(max_length=20)
    order_time = models.DateTimeField(default=datetime.utcnow()+timedelta(hours=1))
    clientOrderId = models.CharField(max_length=20)
    price = models.FloatField()
    qty = models.FloatField()
    status = models.CharField(max_length=20)
    side = models.CharField(choices=SIDE_CHOICES,max_length=10)
    type = models.CharField(max_length=10)
    cancel_time = models.DateTimeField(null=True)
    def __str__(self):
        return f'Order'
    class Meta:
        verbose_name = 'Spot Orders'
        verbose_name_plural = 'Spot Orders'

class Debug(models.Model):
    time = models.DateTimeField(default=datetime.utcnow()+timedelta(hours=1),editable=False)
    error = models.TextField()
    def __str__(self):
        return f'{self.time} {self.error}'
    class Meta:
        verbose_name = 'Debug Logs'
        verbose_name_plural = 'Debug Logs'

class Market_Order(models.Model):
    symbol = models.ForeignKey(Config,on_delete=models.CASCADE)
    orderId = models.CharField(max_length=20)
    order_time = models.DateTimeField(default=datetime.utcnow()+timedelta(hours=1))
    clientOrderId = models.CharField(max_length=20)
    price = models.FloatField()
    qty = models.FloatField()
    status = models.CharField(max_length=20)
    side = models.CharField(choices=SIDE_CHOICES,max_length=10)
    type = models.CharField(max_length=10)
    def __str__(self):
        return f'Order'
    class Meta:
        verbose_name = 'Market Orders'
        verbose_name_plural = 'Market Orders'

class APIlimit(models.Model):
    Order_Count = models.BigIntegerField(default = 0)
    def __str__(self):
        return f'{datetime.utcnow()+timedelta(hours=1)} {self.Order_Count}'
    class Meta:

        verbose_name = 'API Quota'
        verbose_name_plural = 'API Quota'

class Interval(models.Model):
    interval = models.IntegerField()
    run = models.BooleanField(default=True)
    def __str__(self):
        return f'{self.interval} Seconds'


class Logs(models.Model):
    time = models.DateTimeField(default=datetime.utcnow()+timedelta(hours=1))
    symbol = models.ForeignKey(Config,on_delete=models.SET_NULL , null=True)
    message = models.TextField()
    def __str__(self):
        return f'{self.time} {self.symbol} {self.message}'




from .signals import maximum_storage_handler , global_config_updater ,cron_handler , order_cancel_handler,maximum_log_storage_handler
post_save.connect(maximum_storage_handler, sender=Market_Order)
post_save.connect(maximum_storage_handler, sender=Spot_Order )
post_save.connect(maximum_log_storage_handler, sender=Logs)
post_save.connect(maximum_log_storage_handler, sender=Debug)
post_save.connect(global_config_updater, sender=GlobalConfig)
# post_save.connect(cron_handler, sender=Interval)
# post_save.connect(order_cancel_handler,sender = GlobalConfig )