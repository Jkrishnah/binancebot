from django.contrib import admin
from django.contrib.auth.models import Group,User
from bbot.models import *

admin.site.unregister(Group)
admin.site.unregister(User)


admin.site.site_header = 'Binance Administration'
admin.site.index_title = 'Binance Admin'
admin.site.site_title = 'Crypto Bot'

admin.site.register(Debug)

# Register your models here.
@admin.register(GlobalConfig)
class GlobalConfigAdmin(admin.ModelAdmin):
   #   readonly_fields = ["symbol"]
     list_display = ["volume","depth","active","wallet_balance","wait","minimum","order_cancel_time"]
     fields = ["volume","depth","active","wallet_balance","wait","minimum","order_cancel_time"]
   #   def has_add_permission(self, request):
   #      return False
   #   def has_delete_permission(self, request, obj=None):
   #      return False

# Register your models here.
@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
   #   readonly_fields = ["symbol"]
     list_display = ["symbol","dominant_Pair","recessive_Pair","volume","depth","active","wallet_balance","wait","minimum","order_cancel_time"]
     fields = ["symbol","dominant_Pair","recessive_Pair","volume","depth","active","wallet_balance","wait","minimum","order_cancel_time"]
   #   def has_add_permission(self, request):
   #      return False
   #   def has_delete_permission(self, request, obj=None):
   #      return False

@admin.register(LastRunTime)
class lastruntimeAdmin(admin.ModelAdmin):
   #   def has_add_permission(self, request):
   #      return False
     def has_delete_permission(self, request, obj=None):
        return False
     def has_change_permission(self, request, obj=None):
         return False

@admin.register(Key)
class keyAdmin(admin.ModelAdmin):
     def has_add_permission(self, request):
        return False
     def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Logs)
class LogsAdmin(admin.ModelAdmin):
   list_display = ["time","symbol","message"]
   fields = ["time","symbol","message"]
   def has_add_permission(self, request):
      return False
   def has_change_permission(self, request, obj=None):
      return False


@admin.register(Spot_Order)
class spotOrderAdmin(admin.ModelAdmin):
   list_display = ['order_time','symbol','status','price','qty','cancel_time']
   list_filter = ['order_time','symbol','status']
   ordering = ['-order_time']
   def has_add_permission(self, request):
      return False
   def has_change_permission(self, request, obj=None):
      return False

@admin.register(Market_Order)
class marketOrderAdmin(admin.ModelAdmin):
   list_display = ['order_time','symbol','status','price','qty']
   list_filter = ['order_time','symbol','status']
   ordering = ['-order_time']
   def has_add_permission(self, request):
      return False
   def has_change_permission(self, request, obj=None):
      return False

@admin.register(APIlimit)
class APIlimitOrderAdmin(admin.ModelAdmin):
   # list_display = ['Order_Count']
   def has_add_permission(self, request):
      return False
   def has_delete_permission(self, request, obj=None):
      return False
   def has_change_permission(self, request, obj=None):
      return False

@admin.register(Interval)
class IntervalAdmin(admin.ModelAdmin):
   #   def has_add_permission(self, request):
   #      return False
     def has_delete_permission(self, request, obj=None):
        return False
