import os
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'binancebot.settings'
django.setup()
from bbot.models import *
import time
from datetime import datetime,timedelta
from Symbol import Pair

def walletBalance(coinObject):
    if coinObject.currencyBalanceCheck and not coinObject.coinBalanceCheck:
        Logs.objects.create(symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script for {coinObject.symbol} Pair - Recessive pair Insufficient",time=datetime.utcnow()+timedelta(hours=1))
        if ((len(activePairs)+1) * coinObject.minimum) <= coinObject.currencyBalance:  
            Logs.objects.create(symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script Condition Passed {coinObject.symbol} Pair",time=datetime.utcnow()+timedelta(hours=1))
            marketBuyQty = coinObject.currencyBalance / (len(activePairs)+1)
            marketBuyQty = marketBuyQty / coinObject.sellPrice
            marketBuyOrder = coinObject.marketBuyOrder(coinObject.mround_down(marketBuyQty))
            marketBuyOrderObj = Market_Order.objects.create(
                                                        symbol = Config.objects.get(symbol = coinObject.symbol) ,
                                                        orderId = marketBuyOrder['orderId'] ,
                                                        clientOrderId = marketBuyOrder ['clientOrderId'],
                                                        price = marketBuyOrder['price'],
                                                        qty = marketBuyOrder['origQty'],
                                                        status = marketBuyOrder['status'],
                                                        type = marketBuyOrder['type'],
                                                        side = marketBuyOrder['side'],
                                                        order_time=datetime.utcnow()+timedelta(hours=1)        
                                                    )
            Logs.objects.create(symbol = Config.objects.get(symbol = coinObject.symbol),time=datetime.utcnow()+timedelta(hours=1),message=f"Wallet Balance Script - Market Buy Order Created for {coinObject.symbol}")
            return True
        else:
            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Failed {coinObject.symbol} - Insufficient Balance")
            Debug.objects.create(error = f'Wallet Balance Script - insufficient balance',time=datetime.utcnow()+timedelta(hours=1))
            return False

    elif coinObject.coinBalanceCheck and not coinObject.currencyBalanceCheck:
        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script for {coinObject.symbol} Pair - Dominant pair Insufficient")
        if any((coin.coinBalanceInCurrency * 2) >= coin.minimum for coin in coinObjects):
            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script Condition Passed {coinObject.symbol} Pair")
            for coin in coinObjects:
                marketSellOrder = coin.marketSellOrder(coinObject.mround_down(0.5*coin.coinBalance))
                marketSellOrderObj =  Market_Order.objects.create(
                                                        symbol=Config.objects.get(symbol = coin.symbol) ,
                                                        orderId = marketSellOrder['orderId'] ,
                                                        clientOrderId = marketSellOrder ['clientOrderId'],
                                                        price = marketSellOrder['price'],
                                                        qty = marketSellOrder['origQty'],
                                                        status = marketSellOrder['status'],
                                                        type = marketSellOrder['type'],
                                                        side = marketSellOrder['side'],
                                                        order_time=datetime.utcnow()+timedelta(hours=1)         
                                                    )
            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Market Sell Order Created for {coin.symbol}")
            return True
        else:
            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Failed Insufficient Balance for {coinObject.symbol}")
            Debug.objects.create(time=datetime.utcnow()+timedelta(hours=1),error = f'Wallet Balance Script - insufficient balance')
            return False

    

                    #store in database
    else:
        if len(coinObjects) > 1:
            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script for {coinObject.symbol} Pair - Both Dominant & Recessive pair Insufficient")
            otherCoinsBalanceCheck = [(coinObj.coinBalanceInCurrency*2) >= coinObj.minimum for coinObj in coinObjects if not coinObj.symbol == coinObject.symbol ]
            if all(otherCoinsBalanceCheck):
                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script Condition Passed {coinObject.symbol} Pair")
                for coin in coinObjects:
                    if coin.symbol == coinObject.symbol:
                        continue
                    else:
                        marketSellOrder = coin.marketSellOrder(coinObject.mround_down(0.5*coin.coinBalance))
                        marketSellOrderObj =  Market_Order.objects.create(
                                                            symbol = Config.objects.get(symbol = coin.symbol) ,
                                                            orderId = marketSellOrder['orderId'] ,
                                                            clientOrderId = marketSellOrder ['clientOrderId'],
                                                            price = marketSellOrder['price'],
                                                            qty = marketSellOrder['origQty'],
                                                            status = marketSellOrder['status'],
                                                            type = marketSellOrder['type'],
                                                            side = marketSellOrder['side'],
                                                            order_time=datetime.utcnow()+timedelta(hours=1)         
                                                        )
                        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Market Sell Order Created for {coin.symbol}")


                marketBuyQty = coinObject.currencyBalance / (len(activePairs)+1)
                marketBuyQty = marketBuyQty / coinObject.sellPrice
                marketBuyOrder = coinObject.marketBuyOrder(coinObject.mround_down(marketBuyQty))
                marketBuyOrderObj = Market_Order.objects.create(
                                                            symbol = Config.objects.get(symbol = coinObject.symbol) ,
                                                            orderId = marketBuyOrder['orderId'] ,
                                                            clientOrderId = marketBuyOrder ['clientOrderId'],
                                                            price = marketBuyOrder['price'],
                                                            qty = marketBuyOrder['origQty'],
                                                            status = marketBuyOrder['status'],
                                                            type = marketBuyOrder['type'],
                                                            side = marketBuyOrder['side'],
                                                            order_time =datetime.utcnow()+timedelta(hours=1)         
                                                        
                                                        )
                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Market Buy Order Created for {coinObject.symbol}")

                return True
            else:
                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script - Failed Insufficient Balance for {coinObject.symbol}")
                Debug.objects.create(time=datetime.utcnow()+timedelta(hours=1),error = f'Wallet Balance Script - insufficient balance')                
                return False



def trade(coinObject):
    if coinObject.isMarketDepthPositive():
        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Market Positive for {coinObject.symbol} Pair")
        buyOrder,sellOrder = coinObject.spotOrder()
        buyOrderObj = Spot_Order.objects.create(
                                                    symbol = Config.objects.get(symbol = coinObject.symbol) ,
                                                    orderId = buyOrder['orderId'] ,
                                                    clientOrderId = buyOrder ['clientOrderId'],
                                                    price = buyOrder['price'],
                                                    qty = buyOrder['origQty'],
                                                    status = buyOrder['status'],
                                                    type = buyOrder['type'],
                                                    side = buyOrder['side'],
                                                    order_time=datetime.utcnow()+timedelta(hours=1)         
                                                )
        sellOrderObj = Spot_Order.objects.create(
                                                    symbol = Config.objects.get(symbol = coinObject.symbol) ,
                                                    orderId = sellOrder['orderId'] ,
                                                    clientOrderId = sellOrder ['clientOrderId'],
                                                    price = sellOrder['price'],
                                                    qty = sellOrder['origQty'],
                                                    status = sellOrder['status'],
                                                    type = sellOrder['type'],
                                                    side = sellOrder['side'],
                                                    order_time = datetime.utcnow()+timedelta(hours=1)  
                                                )
        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Limit BUY and SELL Orders placed for {coinObject.symbol} Pair")
    else:
        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Market Negative for {coinObject.symbol} Pair")
        time.sleep(coinObject.wait)
while True:
    try:
        scriptEnabled = Interval.objects.first()
        if scriptEnabled.run:
            if APIlimit.objects.first().Order_Count <= 1_98_000 :
                runtime = datetime.utcnow()+timedelta(hours=1)
                lastRuntimeObj = LastRunTime.objects.last()
                lastRuntimeObj.time = runtime
                lastRuntimeObj.save()
                #api key init
                keyObject = Key.objects.first()

                #constants
                activePairs = Config.objects.filter(active = True)
                coinObjects = [Pair(pair,keyObject) for pair in activePairs ]


                for coinObject in coinObjects :
                    Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Trade started for {coinObject.symbol} Pair")
                    if not coinObject.isBalanceSufficient():
                        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Balance in-Sufficient for {coinObject.symbol} Pair ")
                        if coinObject.wallet_balance:
                            if walletBalance(coinObject):
                                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script for {coinObject.symbol} Pair Executed ")
                                if coinObject.isBalanceSufficient():
                                    Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Balance sufficient after Wallet Balance Script for {coinObject.symbol} Pair")
                                    trade(coinObject)
                                else:
                                    Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Balance insufficient after Wallet Balance Script for {coinObject.symbol} Pair")
                            else:
                                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script for {coinObject.symbol} Pair Exited ")
                                continue
                        else:
                            Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Wallet Balance Script Disabled {coinObject.symbol} Pair Exited ")
                            continue

                    else:
                        Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"Trade started for {coinObject.symbol} Pair")
                        trade(coinObject)
            else:
                Logs.objects.create(time=datetime.utcnow()+timedelta(hours=1),symbol = Config.objects.get(symbol = coinObject.symbol),message=f"API Limit crossed {APIlimit.objects.first().Order_Count}")
                # Debug.objects.create(time=datetime.utcnow()+timedelta(hours=1),error = e)

                            
    except Exception as e:
        print(e)
        Debug.objects.create(error = e,time=datetime.utcnow()+timedelta(hours=1))

    time.sleep(Interval.objects.first().interval)


# orders = client.get_all_orders(symbol='USDTNGN', limit=1)
# print(orders)