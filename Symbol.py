from binance.enums import SIDE_BUY,SIDE_SELL,ORDER_TYPE_LIMIT,TIME_IN_FORCE_GTC
from binance.client import Client
import math

class Pair():
    def __init__(self,pair,key):
        #api call 1
        #authentication 
        self.client = Client(key.apiKey , key.apiSecret)
        
        self.symbol = pair.symbol
        self.volume = pair.volume
        self.Dominant_Pair = pair.dominant_Pair
        self.Recessive_Pair = pair.recessive_Pair
        self.wait = pair.wait
        self.depth = pair.depth
        self.wallet_balance = pair.wallet_balance
        self.minimum  = pair.minimum
        #coin balance and dominant pair balance - api calls 2
        self.coinBalance = float(self.client.get_asset_balance(asset=self.Recessive_Pair)['free'])
        self.currencyBalance = float(self.client.get_asset_balance(asset=self.Dominant_Pair)['free'])


       
        
        #market price - api calls 1
        self.lastPrice = self.client.get_ticker(symbol = pair.symbol)
        ##

        self.buyPrice = float(self.lastPrice['bidPrice'])
        self.sellPrice = float(self.lastPrice['askPrice'])
        self.coinToCurrency = self.volume / self.sellPrice
        self.qty = self.mround_down(self.coinToCurrency)
        
        self.coinBalanceInCurrency =  self.sellPrice*self.coinBalance

        self.isBalanceSufficient()

    # #balance checks
    def isBalanceSufficient(self):
        self.coinBalanceCheck = self.sellPrice*self.coinBalance >= self.volume
        self.currencyBalanceCheck = self.currencyBalance >= self.volume
        return self.coinBalanceCheck and self.currencyBalanceCheck
       
    
    def isMarketDepthPositive(self):
        marketDepth = (((self.sellPrice - self.buyPrice) / ((self.sellPrice + self.buyPrice) / 2)) * 100)
        return marketDepth >= self.depth
    
    #Api call 1
    def cancelOrder(self , orderId):
        return self.client.cancel_order(symbol=self.symbol,orderId=orderId)

    def round_down(self,value):
        multiplier = 10 ** 6
        return math.floor(value * multiplier) / multiplier

    def mround_down(self,number):
        info = self.client.get_symbol_info(self.symbol)
        step_size = [float(_['stepSize']) for _ in info['filters'] if _['filterType'] == 'LOT_SIZE'][0]
        step_size = '%.8f' % step_size
        step_size = step_size.rstrip('0')
        decimals = len(step_size.split('.')[1])
        return math.floor(number * 10 ** decimals) / 10 ** decimals

    
    #api calls 2
    def spotOrder(self):
        sellOrder = self.client.create_order(   symbol=self.symbol,
                                                side=SIDE_SELL,
                                                type=ORDER_TYPE_LIMIT,
                                                timeInForce=TIME_IN_FORCE_GTC,
                                                quantity=self.qty,
                                                price=str(self.sellPrice)
                                            )
        buyOrder = self.client.create_order(    symbol=self.symbol,
                                                side=SIDE_BUY,
                                                type=ORDER_TYPE_LIMIT,
                                                timeInForce=TIME_IN_FORCE_GTC,
                                                quantity=self.qty,
                                                price=str(self.buyPrice)
                                            )
        return buyOrder,sellOrder   
    
    #Api call 1
    def marketBuyOrder(self,buyQty, symbol = None):
        symbol = symbol if symbol else self.symbol 
        return self.client.order_market_buy(symbol = self.symbol , quantity = buyQty)

    #Api call 1
    def marketSellOrder(self,sellQty,symbol = None):
        symbol = symbol if symbol else self.symbol 
        return self.client.order_market_sell(symbol = self.symbol , quantity = sellQty)
    
    #Api call 1
    def allOpenOrders(self):
        return self.client.get_open_orders(symbol = self.symbol)

